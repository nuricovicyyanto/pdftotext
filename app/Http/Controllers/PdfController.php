<?php

namespace App\Http\Controllers;

use App\Exports\PdfExport;
use Illuminate\Http\Request;
use Spatie\PdfToText\Pdf;
use Smalot\PdfParser\Parser;
use App\Models\Pdf as PdfModel;
use Maatwebsite\Excel\Facades\Excel;




class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $pdfs = PdfModel::all();
        return view('pdf.index', compact('pdfs'));
    }

    public function export()
    {
        return Excel::download(new PdfExport, 'data.xlsx');
    }

    public function store(Request $request)
    {

        $pdfs = PdfModel::all();

        foreach ($request->file('file') as $file) {
            $pdfParser = new Parser();
            $pdf = $pdfParser->parseFile($file->path());
            $text = $pdf->getText();


            $codePattern = '/(?<=No. Pesanan\s).+/';

            if (preg_match($codePattern, $text, $codeMatches)) {
                $codeBooking = $codeMatches[0];
            } else {
                $codeBooking = null;
            }

            $namePattern = '/(?<=Nama\s).+/';

            if (preg_match($namePattern, $text, $nameMatches)) {
                $fullName = $nameMatches[0];
            } else {
                $fullName = null;
            }

            $phonePattern = '/(\+62|0)8\d{8,10}/';

            if (preg_match($phonePattern, $text, $phoneMatches)) {
                $phoneNumber = $phoneMatches[0];
            } else {
                $phoneNumber = null;
            }

            $statusPattern = '/\bTanpa Supir\b/';

            if (preg_match($statusPattern, $text, $statusMatches)) {
                $statusSupir = $statusMatches[0];
            } else {
                $statusSupir = null;
            }

            $unitPattern = '/Supir\s+(.+)/';

            if (preg_match($unitPattern, $text, $unitMatches)) {
                $unitAlokasi = $unitMatches[0];
                $unitAlokasi = preg_replace('/\bSupir\b\s*/i', '', $unitAlokasi);
                $unitAlokasi = preg_replace('/\s•.*$/', '', $unitAlokasi);
            } else {
                $unitAlokasi = null;
            }

            $cityPattern = '/(?<=rental\s)\S+/';

            if (preg_match($cityPattern, $text, $cityMatches)) {
                $city = $cityMatches[0];
            } else {
                $city = null;
            }

            $transmisiPattern = '/(?<=•\s)\S+/';

            if (preg_match($transmisiPattern, $text, $transmisiMatches)) {
                $transmisi = $transmisiMatches[0];
            } else {
                $transmisi = null;
            }

            $tangjemPattern = '/(?<=Waktu Jemput\s).+/';

            if (preg_match($tangjemPattern, $text, $tangjemMatches)) {
                $tanggalJemput = $tangjemMatches[0];
                $tanggalJemput = preg_replace('/\s•.*$/', '', $tanggalJemput);
            } else {
                $tanggalJemput = null;
            }

            $wakjemPattern = '/(?<=Waktu Jemput\s).+/';

            if (preg_match($wakjemPattern, $text, $wakjemMatches)) {
                $waktuJemput = $wakjemMatches[0];
                $waktuJemput = preg_replace('/^.*•\s(\d{2}\.\d{2}).*$/', '$1', $waktuJemput);
            } else {
                $waktuJemput = null;
            }

            $tangpengPattern = '/(?<=Tanggal & waktu pengembalian\s).+/';

            if (preg_match($tangpengPattern, $text, $tangpengMatches)) {
                $tanggalPengembalian = $tangpengMatches[0];
                $tanggalPengembalian = preg_replace('/\s•.*$/', '', $tanggalPengembalian);
            } else {
                $tanggalPengembalian = null;
            }

            $wakpengPattern = '/(?<=Tanggal & waktu pengembalian\s).+/';

            if (preg_match($wakpengPattern, $text, $wakpengMatches)) {
                $waktuPengembalian = $wakpengMatches[0];
                $waktuPengembalian = preg_replace('/^.*•\s(\d{2}\.\d{2}).*$/', '$1', $waktuPengembalian);
            } else {
                $waktuPengembalian = null;
            }

            $lokpenjPattern = '/(?<=Lokasi Jemput\s).+/';

            if (preg_match($lokpenjPattern, $text, $lokpenjMatches)) {
                $lokasiPenjemputan = $lokpenjMatches[0];
            } else {
                $lokasiPenjemputan = null;
            }

            $lokpengPattern = '/(?<=Lokasi pengembalian\s).+/';

            if (preg_match($lokpengPattern, $text, $lokpengMatches)) {
                $lokasiPengembalian = $lokpengMatches[0];
            } else {
                $lokasiPengembalian = null;
            }


            // If a record does not exist, save the new record
            $existingPdf = PdfModel::where('code', $codeBooking)->first();
            if (!$existingPdf) {
                $pdf = new PdfModel();
                $pdf->code = $codeBooking;
                $pdf->fullname = $fullName;
                $pdf->phonenumber = " " . $phoneNumber;
                $pdf->statusSopir = $statusSupir;
                $pdf->unitAlokasi = $unitAlokasi;
                $pdf->city = $city;
                $pdf->transmisi = $transmisi;
                $pdf->tanggalJemput = $tanggalJemput;
                $pdf->waktuJemput = " " . $waktuJemput;
                $pdf->tanggalPengembalian = $tanggalPengembalian;
                $pdf->waktuPengembalian = " " . $waktuPengembalian;
                $pdf->lokasiPenjemputan = $lokasiPenjemputan;
                $pdf->lokasiPengembalian = $lokasiPengembalian;
                $pdf->save();
            }
        }

        return redirect()->route('file')->withcompact('pdfs')->with('success', 'File uploaded successfully!');
    }


    public function deleteAll()
    {
        PdfModel::truncate();
        return redirect()->back()->with('success', 'All data has been removed successfully.');
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $dok = PdfModel::findorfail($id);
        $dok->delete();
        return back();
    }
}
