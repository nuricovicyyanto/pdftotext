<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pdf extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'fullname',
        'phoneNumber',
        'statusSopir',
        'unitAlokasi',
        'city',
        'transmisi',
        'tanggalJemput',
        'waktuJemput',
        'tanggalPengembalian',
        'waktuPengembalian',
        'lokasiPenjemputan',
        'lokasiPengembalian'
    ];
}
