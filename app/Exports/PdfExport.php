<?php

namespace App\Exports;

use App\Models\Pdf;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PdfExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Pdf::select(
            'code',
            'fullname',
            'phoneNumber',
            'statusSopir',
            'unitAlokasi',
            'city',
            'transmisi',
            'tanggalJemput',
            'waktuJemput',
            'tanggalPengembalian',
            'waktuPengembalian',
            'lokasiPenjemputan',
            'lokasiPengembalian'
        )->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Code',
            'Full Name',
            'Phone Number',
            'Driver Status',
            'Allocation Unit',
            'City',
            'Transmission',
            'Pickup Date',
            'Pickup Time',
            'Return Date',
            'Return Time',
            'Pickup Location',
            'Return Location'
        ];
    }
}
