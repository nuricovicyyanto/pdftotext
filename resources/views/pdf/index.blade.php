<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pdf to Text</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">

    <!-- jQuery -->
    <script type="text/javascript" src="//code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- DataTables JS -->
    <script type="text/javascript" src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

</head>

<body>
    <div class="container">
        <div class="row align-items-center vh-100">
            <div class="col-6 mx-auto">
                <div class="card shadow border">
                    <div class="card-body d-flex flex-column align-items-center">
                        <div class="d-flex justify-content-center">
                            <form action="{{ route('file.store') }}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file[]" max="20" multiple>
                                </div><br>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                        
                        <a href="{{ route('export') }}" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
                        <form action="{{ route('pdf.deleteAll') }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete All</button>
                        </form>   
                    </div>
                </div>             
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="pdfs-table" class="table">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Code</th>
                                    <th>Fullname</th>
                                    <th>Phone Number</th>
                                    <th>Status Sopir</th>
                                    <th>Unit Alokasi</th>
                                    <th>City</th>
                                    <th>Transmisi</th>
                                    <th>Tanggal Jemput</th>
                                    <th>Waktu Jemput</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Waktu Pengembalian</th>
                                    <th>Lokasi Penjemputan</th>
                                    <th>Lokasi Pengembalian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pdfs as $pdf)
                                    <tr>
                                        <td>
                                            <a href="{{ url('destroyData', $pdf->id) }}" type="button"
                                                class="btn btn-danger" data-toggle="edit-atas" data-placement="right"
                                                title="Hapus data">Hapus</a>
                                        </td>
                                        <td>{{ $pdf->code }}</td>
                                        <td>{{ $pdf->fullname }}</td>
                                        <td>{{ $pdf->phoneNumber }}</td>
                                        <td>{{ $pdf->statusSopir }}</td>
                                        <td>{{ $pdf->unitAlokasi }}</td>
                                        <td>{{ $pdf->city }}</td>
                                        <td>{{ $pdf->transmisi }}</td>
                                        <td>{{ $pdf->tanggalJemput }}</td>
                                        <td>{{ $pdf->waktuJemput }}</td>
                                        <td>{{ $pdf->tanggalPengembalian }}</td>
                                        <td>{{ $pdf->waktuPengembalian }}</td>
                                        <td>{{ $pdf->lokasiPenjemputan }}</td>
                                        <td>{{ $pdf->lokasiPengembalian }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function() {
                    var table = $('#pdfs-table').DataTable({
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'colvis',
                            text: 'Columns',
                            postfixButtons: ['colvisRestore'],
                        }, ],
                    });
                });
            </script>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
</body>

</html>
