<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pdfs', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('fullname')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('statusSopir')->nullable();
            $table->string('unitAlokasi')->nullable();
            $table->string('city')->nullable();
            $table->string('transmisi')->nullable();
            $table->string('tanggalJemput')->nullable();
            $table->string('waktuJemput')->nullable();
            $table->string('tanggalPengembalian')->nullable();
            $table->string('waktuPengembalian')->nullable();
            $table->string('lokasiPenjemputan')->nullable();
            $table->string('lokasiPengembalian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pdfs');
    }
};
