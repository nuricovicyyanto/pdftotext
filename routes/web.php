<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [PdfController::class, 'index'])->name('file');
Route::post('file', [PdfController::class, 'store'])->name('file.store');
Route::get('/destroyData/{id}', [PdfController::class, 'destroy'])->name('destroyData');
Route::get('export', [PdfController::class, 'export'])->name('export');
Route::delete('/pdfs/delete-all', [PdfController::class, 'deleteAll'])->name('pdf.deleteAll');

